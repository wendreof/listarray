package info.dreamsoftwares.listaarray.model;

import java.io.Serializable;

public class Conta implements Serializable{

    private int numero;
    private Cliente cli;
    private double saldo;

    public Conta() {
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cliente getCli() {
        return cli;
    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean sacar(double valor)
    {
        if ( saldo > 0 )
        {
            this.saldo -= valor;

        }
       return false;
    }

    public void depositar(double valor)
    {
        if ( valor > 0)
        {
            this.saldo += valor;
        }
    }
}
