package info.dreamsoftwares.listaarray.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Pedido implements Serializable {

    private Cliente cli;
    private List<Produto> produtos;
    private double valorTotal;

    public Pedido() {
        this.cli = new Cliente();
        this.produtos = new ArrayList<Produto>();
        this. valorTotal = 0.00;
    }

    public void adicionarCliente(Cliente cli)
    {
        this.cli = cli;
    }

    public void adicionarProduto(Produto produto)
    {
        this.valorTotal+=produto.getValor();
        this.produtos.add(produto);
    }

    public String exibirDadosCadastrados()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Dados do Pedido");
        sb.append("\nCliente:");
        sb.append("\nId: "+cli.getId());
        sb.append("\nNome: "+cli.getNome());
        sb.append("\nEmail: "+cli.getEmail());
        sb.append("\nCPF: "+cli.getCpf());
        sb.append("\nTelefone: "+cli.getTelefone());

        sb.append("\nProdutos: ");

        for(Produto p:produtos)
        {
            sb.append("\nId: "+p.getId());
            sb.append("\nNome: "+p.getNome());
            sb.append("\nValor: "+p.getValor());

        }
        sb.append("\nTotal do Pedido: "+this.valorTotal);

        return sb.toString();
    }

    public Cliente getCli() {
        return cli;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public double getValorTotal() {
        return valorTotal;
    }
}
