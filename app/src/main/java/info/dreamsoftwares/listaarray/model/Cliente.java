package info.dreamsoftwares.listaarray.model;

import java.io.Serializable;

public class Cliente implements Serializable {
    private String nome,cpf,email,telefone;
    private int id;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Nome: " + nome +  "\n" +
                "CPF: " + cpf +  "\n" +
                "E-mail: " + email +  "\n" +
                "Telefone: " + telefone;
    }
}
