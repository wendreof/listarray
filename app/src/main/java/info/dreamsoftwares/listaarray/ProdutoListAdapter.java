package info.dreamsoftwares.listaarray;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import info.dreamsoftwares.listaarray.model.Produto;


public class ProdutoListAdapter extends BaseAdapter {

	private Context context;
	private List<Produto> lista;

	public ProdutoListAdapter(Context context, List<Produto> lista) {
		this.context = context;
		this.lista = lista;
	}

	public int getCount() {
		return lista.size();
	}

	public Object getItem(int position) {
		return lista.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Produto p = lista.get(position);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.lista_produtos, null);
		
		TextView id = (TextView) view.findViewById(R.id.txtIdProdutoLista);
		id.setText(String.valueOf(p.getId()));
		
		TextView nome = (TextView) view.findViewById(R.id.txtNomeProdutoLista);
		nome.setText(p.getNome());
		

		TextView valor = (TextView) view.findViewById(R.id.txtValorLista);
		NumberFormat nf = DecimalFormat.getCurrencyInstance();
		valor.setText(nf.format(p.getValor()));
		
		return view;
	}
}