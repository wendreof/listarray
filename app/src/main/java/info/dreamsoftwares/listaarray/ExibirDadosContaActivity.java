package info.dreamsoftwares.listaarray;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import info.dreamsoftwares.listaarray.model.Conta;

public class ExibirDadosContaActivity extends AppCompatActivity implements Serializable, View.OnClickListener {

    Conta conta;

    private TextView cli;
    private TextView nomeConta;
    private TextView saldoConta;

    private Button btnSacar;
    private Button btnDepositar;

    private EditText value;
    private double newValue;
    private TextView newSaldo;
    private double res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exibir_dados_conta);

        cli = findViewById(R.id.dadoConta);
        nomeConta = findViewById(R.id.nomeConta);
        saldoConta = findViewById(R.id.saldoConta);

        conta = new Conta();

        preencherDados();

        btnSacar = findViewById(R.id.btn_sacar);
        btnDepositar = findViewById(R.id.btn_depositar);
        value = findViewById(R.id.edit_value_sacar_ou_deposito);
        newSaldo = findViewById(R.id.text_new_saldo);

        btnSacar.setOnClickListener(this);
        btnDepositar.setOnClickListener(this);
    }

    public void preencherDados() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            conta = (Conta) extras.getSerializable("conta");

            if (conta != null) {
                cli.setText(String.valueOf(conta.getNumero()));
                saldoConta.setText(String.valueOf(conta.getSaldo()));
                nomeConta.setText(String.valueOf(conta.getCli()));
            }
        }
    }

    @Override
    public void onClick(View view) {

        newValue = Double.parseDouble(value.getText().toString());

        int a = view.getId();
        if (a == R.id.btn_sacar) {
            //logica saque
            conta.sacar(newValue);
            res = conta.getSaldo();
            exibirToast("Saque realizado, seu saldo agora é: ", res);

            newSaldo.setText("" + res);

        } else {
            //logica deposito;
            conta.depositar(newValue);
            res = conta.getSaldo();
            exibirToast("Depósito realizado, seu saldo agora é: ", res);

            newSaldo.setText("" + res);
        }
    }

    private void exibirToast(String msg, double valor) {
        Toast.makeText(this, msg + valor, Toast.LENGTH_LONG).show();
    }
}
