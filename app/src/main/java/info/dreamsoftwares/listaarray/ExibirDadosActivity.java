package info.dreamsoftwares.listaarray;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import info.dreamsoftwares.listaarray.model.Pedido;

public class ExibirDadosActivity extends Activity {

    private TextView idCliente, nomeCliente, cpf, telefone, email, idProduto, nomeProduto, valor, totalPedido;
    private Pedido pedido;
    private ListView lvProdutosPedido;
    private ProdutoListAdapter pla;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exibir_dados);

        idCliente = (TextView) findViewById(R.id.txtIdCliente);
        nomeCliente = (TextView) findViewById(R.id.txtNomeCliente);
        cpf = (TextView) findViewById(R.id.txtCpf);
        telefone = (TextView) findViewById(R.id.txtTelefone);
        email = (TextView) findViewById(R.id.txtEmail);
        idProduto = (TextView) findViewById(R.id.txtIdProdutoLista);
        nomeProduto = (TextView) findViewById(R.id.txtNomeProdutoLista);
        valor = (TextView) findViewById(R.id.txtValorLista);
        totalPedido = (TextView) findViewById(R.id.txtValorPedido);
        lvProdutosPedido = (ListView) findViewById(R.id.lvListaProdutos);

        pedido = new Pedido();
        preencherDados();
    }

    private void preencherDados() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            pedido = (Pedido) extras.getSerializable("pedido");

            if (pedido != null) {
                idCliente.setText(String.valueOf(pedido.getCli().getId()));
                nomeCliente.setText(pedido.getCli().getNome());
                cpf.setText(pedido.getCli().getCpf());
                telefone.setText(pedido.getCli().getTelefone());
                email.setText(pedido.getCli().getEmail());


                pla = new ProdutoListAdapter(this, pedido.getProdutos());
                lvProdutosPedido.setAdapter(pla);
                NumberFormat nf = DecimalFormat.getCurrencyInstance();
                totalPedido.setText(nf.format(pedido.getValorTotal()));
            }
        }
    }
}
