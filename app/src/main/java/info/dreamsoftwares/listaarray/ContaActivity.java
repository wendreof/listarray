package info.dreamsoftwares.listaarray;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

import info.dreamsoftwares.listaarray.model.Cliente;
import info.dreamsoftwares.listaarray.model.Conta;

public class ContaActivity extends AppCompatActivity implements Serializable, View.OnClickListener {

    private Cliente cliente;
    EditText idCli, nomeCli, telefoneCli, emailCli, cpfCli, numConta, saldoCli;
    Button btnCad, btnPreencher;
    private Conta conta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conta);

        idCli = findViewById(R.id.conta_id_cliente);
        nomeCli = findViewById(R.id.conta_nome_cliente);
        telefoneCli = findViewById(R.id.conta_telefone_cliente);
        emailCli = findViewById(R.id.conta_email_cliente);
        cpfCli = findViewById(R.id.conta_cpf_cliente);
        saldoCli = findViewById(R.id.conta_saldo_cliente);

        numConta = findViewById(R.id.edit_num_conta);

        btnCad = findViewById(R.id.btn_cadastrar_conta);
        btnPreencher = findViewById(R.id.btn_preencher_campos);

        conta = new Conta();

        btnCad.setOnClickListener(this);
        btnPreencher.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        int a = view.getId();
        if (a == R.id.btn_cadastrar_conta) {
            exibirToast("Conta cadastrada com sucesso");
            adicionarConta();
            exibirDadosConta();
        } else {
            preencherCampos();
        }
    }

    public void adicionarConta()
    {
        cliente = new Cliente();
        cliente.setId(Integer.parseInt(idCli.getText().toString()));
        cliente.setNome(nomeCli.getText().toString());
        cliente.setCpf(cpfCli.getText().toString());
        cliente.setTelefone(telefoneCli.getText().toString());
        cliente.setEmail(emailCli.getText().toString());

        conta.setNumero(Integer.parseInt(numConta.getText().toString()));
        conta.setSaldo(Double.parseDouble(saldoCli.getText().toString()));

        conta.setCli(cliente);
        exibirToast("Conta adicionada");
    }

    public void exibirDadosConta()
    {
        Intent it = new Intent(this, ExibirDadosContaActivity.class);
        it.putExtra("conta", conta);
        startActivity(it);
    }

    private void exibirToast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private void preencherCampos()
    {
        idCli.setText("01234");
        nomeCli.setText("Eloá Vitória de Fátima Neves Oliveira");
        cpfCli.setText("417.201.638-39");
        telefoneCli.setText("(19) 99343-2853");
        emailCli.setText("eloavitoria@gmail.com");
        numConta.setText("1993");
        saldoCli.setText("500");
    }
}
