package info.dreamsoftwares.listaarray;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import info.dreamsoftwares.listaarray.model.Cliente;
import info.dreamsoftwares.listaarray.model.Pedido;
import info.dreamsoftwares.listaarray.model.Produto;

public class MainActivity extends Activity {

    private EditText idCliente, nomeCliente, cpf, telefone, email, idProduto, nomeProduto, valor;
    private Cliente cliente;
    private Produto produto;
    private Pedido pedido;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        idCliente = (EditText) findViewById(R.id.edtIdCliente);
        nomeCliente = (EditText) findViewById(R.id.edtNomeCliente);
        cpf = (EditText) findViewById(R.id.edtCpf);
        telefone = (EditText) findViewById(R.id.edtTelefone);
        email = (EditText) findViewById(R.id.edtEmail);
        idProduto = (EditText) findViewById(R.id.edtIdProduto);
        nomeProduto = (EditText) findViewById(R.id.edtNomeProduto);
        valor = (EditText) findViewById(R.id.edtValor);

        /*valor.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)) {
                    Locale myLocale = new Locale("pt", "BR");
                    //Nesse bloco ele monta a maskara para money
                    valor.removeTextChangedListener(this);
                    String cleanString = s.toString().replaceAll("[R$,.]", "");
                    Double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance(myLocale).format((parsed / 100));
                    current = formatted;
                    valor.setText(formatted);
                    valor.setSelection(formatted.length());
                    valor.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        pedido = new Pedido();

    }

    public void adiconarCliente(View v)
    {
        cliente = new Cliente();
        cliente.setId(Integer.parseInt(idCliente.getText().toString()));
        cliente.setNome(nomeCliente.getText().toString());
        cliente.setCpf(cpf.getText().toString());
        cliente.setTelefone(telefone.getText().toString());
        cliente.setEmail(email.getText().toString());

        pedido.adicionarCliente(cliente);
        exibirToast("Cliente adicionado com sucesso!");

    }

    public void adicionarProduto(View v)
    {
        produto = new Produto();
        produto.setId(Integer.parseInt(idProduto.getText().toString()));
        produto.setNome(nomeProduto.getText().toString());
        produto.setValor(Double.parseDouble(valor.getText().toString()));

        pedido.adicionarProduto(produto);
        exibirToast("Produto adicionado com sucesso!");
    }

    public void exibirDadosPedido(View v)
    {
        /*exibirToast(pedido.exibirDadosCadastrados());*/
        Intent it = new Intent(this, ExibirDadosActivity.class);
        it.putExtra("pedido",pedido);
        startActivity(it);
    }

    private void exibirToast(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }
}
